/*
 * TestValuesParser.c
 *
 *  Created on: 24 апр. 2018 г.
 *      Author: svetozar
 */


#include <stdlib.h>
#include <SDR/TestBase/TestValues.h>

bool test_str_isEqual(const char * val, const char * str)
{
  return strcmp(val, str) == 0;
}

int test_parse_int(const char * val)
{
  return atoi(val);
}

long double test_parse_float(const char * val)
{
  return strtold(val,0);
}

Sample_t test_parse_sample(const char * val)
{
  return strtold(val,0);
}

Size_t test_parse_samples(const char * val, Sample_t * buf, Size_t maxsize)
{
  int i = 0;
  char * end_ptr = (char*)val;
  while(*end_ptr != '\0')
  {
    if (i > maxsize)
      return 0;
    char *start_ptr = end_ptr;

    Sample_t x = strtold(start_ptr,&end_ptr);
    if (end_ptr == start_ptr)
      break;
    buf[i++] = x;
  }
  return i;
}
