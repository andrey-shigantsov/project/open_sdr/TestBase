/*
 * TestMethod.h
 *
 *  Created on: 26 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_TESTMETHOD_H_
#define SYS_TESTMETHOD_H_

#include <SDR/BASE/common.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef void (*test_get_format_fxn_t)(void * This, char * buf, Size_t size);
typedef void (*test_sys_init_fxn_t)(void * This);
typedef void (*test_sys_wakeup_fxn_t)(void * This);
typedef void (*test_sys_destroy_fxn_t)(void * This);
typedef void (*test_refresh_state_fxn_t)(void * This);
typedef void (*test_prestart_fxn_t)(void * This);
typedef void (*test_exec_fxn_t)(void * This);
typedef void (*test_reset_fxn_t)(void * This);
typedef void (*test_prestop_fxn_t)(void * This);
typedef const char * (*test_value_id_str_fxn_t)(void * This, UInt8_t id);
typedef void (*test_set_value_fxn_t)(void * This, UInt8_t id, const char * val, Size_t size);
typedef struct
{
  test_get_format_fxn_t get_control_format;
  test_get_format_fxn_t get_status_format;
  test_get_format_fxn_t get_plots_format;
  test_sys_init_fxn_t sys_init;
  test_sys_wakeup_fxn_t sys_wakeup;
  test_sys_destroy_fxn_t sys_destroy;
  test_refresh_state_fxn_t refresh_state;
  test_prestart_fxn_t prestart;
  test_exec_fxn_t exec;
  test_reset_fxn_t reset;
  test_prestop_fxn_t prestop;
  test_value_id_str_fxn_t value_id_str;
  test_set_value_fxn_t set_value;
} TestMethod_t;

INLINE void init_TestMetod(TestMethod_t * This){INIT_STRUCT(TestMethod_t,This);}

#ifdef __cplusplus
}
#endif

#endif /* SYS_TESTMETHOD_H_ */
